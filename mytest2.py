from __future__ import division
from nltk.corpus import wordnet as wn
dog = wn.synset('good.n.01')
cat = wn.synset('nice.n.01')
import Levenshtein

print(dog.path_similarity(cat))
#print(dog.lch_similarity(cat))
#print(dog.wup_similarity(cat))

abbott = ['good', 'nice', 'better', 'iitt', 'impact', 'health care']
cyber = ['fine', 'proficient', 'sound', 'iitt', 'good', 'programming', 'health']
#abbott = ['mad', 'memory', 'experience']
#cyber = ['love', 'study', 'enjoy']
count = 0
for abbott_word in abbott:
    for cyber_word in cyber:
		try:
			x = wn.synset(abbott_word+'.n.01')
			y = wn.synset(cyber_word+'.n.01')
			wordnet_simility = x.path_similarity(y)
			lvr = Levenshtein.ratio(abbott_word, cyber_word)
			if wordnet_simility > 0:
				count += 1
				print abbott_word + ' wrd ' + cyber_word + ' '+ str(wordnet_simility)
				break
			else:
				if lvr == 1:
					count += 1
					print abbott_word + ' lvr ' + cyber_word + ' '+ str(lvr)	
					break
		except Exception as e:
			z = 10
percent = count/len(abbott)
print 'cyber' + ' ' + 'abbott ' +  str(percent*100)
