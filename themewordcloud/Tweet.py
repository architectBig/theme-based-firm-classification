import got
from langdetect import detect
class Tweet(object):
	def __init__(self,startDate, endDate):
		self.startDate = startDate
		self.endDate = endDate
	
	def isEnglish(self, text):
		try:
			return detect(text)
		except Exception:
			return 'null'

	def mergeHashtags(self, hashtaglist, hashtagstring):
		currenttaglist = hashtagstring.split()
		for hashtag in currenttaglist:
			if hashtag not in hashtaglist:
				hashtaglist.append(hashtag.lower())
		return hashtaglist

	def getTweets(self, searchQuery, number):
		tweetCriteria = got.manager.TweetCriteria().setQuerySearch(searchQuery).setSince(self.startDate).setUntil(self.endDate).setMaxTweets(number)
		
		tweets = []
		try:
			tweets = got.manager.TweetManager.getTweets(tweetCriteria)
		except Exception:
			print "	Exception occured.. Trying " + searchQuery + " again."
			getTweets(searchQuery, number)

		tweetHashtagsList = []
		number_of_tweets = 0
		for tweet in tweets:
				if self.isEnglish(tweet.text) == 'en':
					number_of_tweets += 1
					tweet_hashtag_list = tweet.hashtags.encode('ascii', 'ignore').split()
					for hashtag in tweet_hashtag_list:
						if hashtag[1:].lower() != searchQuery and hashtag[1:] !='':
							tweetHashtagsList.append(hashtag[1:].lower())
							
		tweetInfo = []
		tweetInfo.append(str(number_of_tweets))
		tweetInfo.append(tweetHashtagsList)
		return tweetInfo
