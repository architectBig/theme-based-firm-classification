from Tweet import Tweet
from collections import Counter

class ThemewordCloud(object):
	def __init__(self, theme, start_date, end_date):
		self.theme = theme
		self.start_date = start_date
		self.end_date = end_date
	def generate_cloud_map(self):
		tweetObj = Tweet(self.start_date, self.end_date)
		tweets = tweetObj.getTweets(self.theme, 1500)
		self.no_tweets = tweets[0]
		self.cloud_map = Counter(tweets[1]).items()
		return self
		
