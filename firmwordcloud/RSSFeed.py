import xml.etree.ElementTree as ET
import urllib2
class RSSFeed (object):
	def __init__(self, base_url, search_key, pub_date):
		self.search_key = search_key
		self.base_url = base_url
		self.pub_date = pub_date
		self.article_link_list = []
	def get_articles(self):
		url = self.base_url + self.search_key+'&region=US&lang=en-US'
		req = urllib2.Request(url)
		try:
			response = urllib2.urlopen(req)
			rss_feed_string = response.read()
			
			root = ET.fromstring(rss_feed_string)
			
			element = root.find('channel')
			
			for item in element.findall('item'):
				link = item.find('link').text
				#pub_date = item.find('pubDate').text
				#print pub_date
				self.article_link_list.append(link)
			return self.article_link_list
			
		except URLError as e:
			print e
		
		
