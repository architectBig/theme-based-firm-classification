import csv
class FirmWordCloudCSV(object):
	def __init__(self, csv):
		file_name = csv + '.csv'
		self.csv_file = open(file_name, 'a+')
	def append(self, firm_word_cloud):
		writer = csv.writer(self.csv_file, quoting=csv.QUOTE_ALL)
		firm_word_cloud_list_style = []
		firm_word_cloud_list_style.append(firm_word_cloud.firm_name)
		firm_word_cloud_list_style.append(firm_word_cloud.date)
		firm_word_cloud_list_style.append(firm_word_cloud.no_articles)
		for dic in firm_word_cloud.frequency_distribution:
				firm_word_cloud_list_style.append(dic[0])
				firm_word_cloud_list_style.append(dic[1])
		writer.writerow(firm_word_cloud_list_style)
		self.csv_file.close()
