import textmining
import nltk
from nltk.stem.snowball import SnowballStemmer
from operator import itemgetter
from nltk.stem import WordNetLemmatizer

class TermDocumentMatrix (object):
	def __init__(self, article_text_list):
		self.article_text_list = article_text_list
		self.frequency_distribution = []
	def tokenize_and_stem(self, text):
		tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
		#stemmer = SnowballStemmer("english")
		wordnet_lemmatizer = WordNetLemmatizer()
		#stems = [stemmer.stem(t) for t in tokens]
		lemmas = [wordnet_lemmatizer.lemmatize(t) for t in tokens]
		#stems_string = ' '.join(stems)
		lemmas_string = ' '.join(lemmas)
		#return stems_string
		return lemmas_string
		
	def get_frequency_distribution (self):
		# Initialize class to create term-document matrix
		tdm = textmining.TermDocumentMatrix()
		# Add the documents
		for doc in self.article_text_list:
			tdm.add_doc(self.tokenize_and_stem(doc))
		
		# this will hold tdm as array
		tdm_array = []
		# list of features
		words_in_tdm = []
	
		counter = 0
	
		for row in tdm.rows(cutoff=1):
			if counter != 0:
				tdm_array.append(row)
			else:
				words_in_tdm = row
			counter += 1
	
		frequency_in_all_documents = []
		
		for i in range (0,len(words_in_tdm)):
			sum_all_document = 0
			for j in range(0,counter -1):
				sum_all_document += tdm_array[j][i]
			frequency_in_all_documents.append(sum_all_document)
	
	
		tagged_words_in_tdm = nltk.pos_tag(words_in_tdm)
		frequency_distribution = zip(tagged_words_in_tdm, frequency_in_all_documents)
		firm_list = ['abbott', 'maboot']
		common_word_list = ['date','today', 'yesterday', 'year', 'month', 'friday', 'saturday', 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december']
		filtered_frequency_distribution = [(tupple[0][0], tupple[1]) for tupple in frequency_distribution if (tupple[0][1] == "NN" or tupple[0][1] == "NNS" or tupple[0][1] == "NNP") and tupple[1] > 20 and tupple [0] [0] not in firm_list and tupple [0] [0] not in common_word_list]
		self.frequency_distribution = filtered_frequency_distribution
		
		return sorted(self.frequency_distribution,key=itemgetter(1))
