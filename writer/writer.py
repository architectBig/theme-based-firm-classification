import csv
class CSV (object):
	def __init__ (self, directory, filename, mode):
		self.directory = directory
		self. filename = filename
		self.mode = mode
	def write(self, content):
		with open(self.directory + self.filename, self.mode) as out:
			writer = csv.writer(out, quoting=csv.QUOTE_ALL)
			writer.writerow(content)

			
		
