from collections import Counter

class WordCloud(object):
	def __init__(self, firm_name, date, no_articles, frequency_distribution):
		self.firm_name = firm_name
		self.date = date
		self.no_articles = no_articles
		self.frequency_distribution = frequency_distribution
		
