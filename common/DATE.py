import datetime
class DATE (object):
	def __init__(self):
		today = datetime.datetime.today().strftime('%Y-%m-%d')
		week_ago = datetime.datetime.today() - datetime.timedelta(days=7)
		week_ago = week_ago.strftime('%Y-%m-%d')
		self.start_date = week_ago
		self.end_date = today
