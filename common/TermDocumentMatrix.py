import textmining
import nltk
from nltk.stem.snowball import SnowballStemmer
from operator import itemgetter
from nltk.stem import WordNetLemmatizer

class TermDocumentMatrix (object):
	def __init__(self, article_text_list):
		self.article_text_list = article_text_list
		self.frequency_distribution = []
		
		firm_name_file = open('common/firm_list/ticker_list.txt', 'r')
		file_content = firm_name_file.readlines()

		self.firm_ticker_list = [ line.strip().lower() for line in file_content]

		firm_name_file = open('common/firm_list/shortname_list.txt', 'r')
		file_content = firm_name_file.readlines()

		self.firm_shortname_list = [ line.strip().lower() for line in file_content]
		self.wordnet_lemmatizer = WordNetLemmatizer()
		
	def tokenize_and_lemma(self, text):
		tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
		lemmas = [self.wordnet_lemmatizer.lemmatize(t) for t in tokens]
		lemmas_string = ' '.join(lemmas)
		return lemmas_string
		
	def tokenize_and_tagging(self, text):
		tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
		tagged_tokens = nltk.pos_tag(tokens)
		NOUN_tagged_tokens = [tagged_token[0] for tagged_token in tagged_tokens if tagged_token[1] == "NN" or tagged_token[1] == "NNS" or tagged_token[1] == "NNP"]
		lemmas = [self.wordnet_lemmatizer.lemmatize(NOUN_tagged_token) for NOUN_tagged_token in NOUN_tagged_tokens]
		tokens_string = ' '.join(token for token in lemmas if (token.encode('ascii','ignore').lower() in self.firm_ticker_list or token.encode('ascii','ignore').lower() in self.firm_shortname_list))
		return tokens_string
		
	def get_keywords_frequency_distribution (self):
		# Initialize class to create term-document matrix
		tdm = textmining.TermDocumentMatrix()
		# Add the documents
		for doc in self.article_text_list:
			tdm.add_doc(self.tokenize_and_lemma(doc))
		
		# this will hold tdm as array
		tdm_array = []
		# list of features
		words_in_tdm = []
	
		counter = 0
	
		for row in tdm.rows(cutoff=1):
			if counter != 0:
				tdm_array.append(row)
			else:
				words_in_tdm = row
			counter += 1
	
		frequency_in_all_documents = []
		
		for i in range (0,len(words_in_tdm)):
			sum_all_document = 0
			for j in range(0,counter -1):
				sum_all_document += tdm_array[j][i]
			frequency_in_all_documents.append(sum_all_document)
	
	
		tagged_words_in_tdm = nltk.pos_tag(words_in_tdm)
		frequency_distribution = zip(tagged_words_in_tdm, frequency_in_all_documents)
		common_word_list = ['date','today', 'yesterday', 'year', 'month', 'friday', 'saturday', 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december']
		filtered_frequency_distribution = [(tupple[0][0], tupple[1]) for tupple in frequency_distribution if (tupple[0][1] == "NN" or tupple[0][1] == "NNS" or tupple[0][1] == "NNP") and tupple[1] > 2]
		self.frequency_distribution = [tupple for tupple in filtered_frequency_distribution if tupple[0].encode('ascii','ignore') not in self.firm_ticker_list and tupple[0].encode('ascii','ignore') not in self.firm_shortname_list and tupple[0].encode('ascii','ignore') not in common_word_list]
		
		return sorted(self.frequency_distribution,key=itemgetter(1), reverse=True)
		
	def get_cooccured_firms_frequency_distribution (self, ticker, shortname):
		# Initialize class to create term-document matrix
		tdm = textmining.TermDocumentMatrix()
		# Add the documents
		for doc in self.article_text_list:
			tdm.add_doc(self.tokenize_and_tagging(doc))
		
		# this will hold tdm as array
		tdm_array = []
		# list of features
		words_in_tdm = []
	
		counter = 0
	
		for row in tdm.rows(cutoff=1):
			if counter != 0:
				tdm_array.append(row)
			else:
				words_in_tdm = row
			counter += 1
	
		frequency_in_all_documents = []
		
		for i in range (0,len(words_in_tdm)):
			sum_all_document = 0
			for j in range(0,counter -1):
				sum_all_document += tdm_array[j][i]
			frequency_in_all_documents.append(sum_all_document)
	
	
		#tagged_words_in_tdm = nltk.pos_tag(words_in_tdm)
		frequency_distribution = zip(words_in_tdm, frequency_in_all_documents)
		filtered_frequency_distribution = [(tupple[0].upper(),tupple[1]) for tupple in frequency_distribution if tupple[1] > 2 and tupple[0] != shortname and tupple[0] != ticker ]
		self.frequency_distribution = filtered_frequency_distribution
		
		return sorted(self.frequency_distribution,key=itemgetter(1), reverse=True)
