import urllib2
from readability.readability import Document
from bs4 import BeautifulSoup
class Article (object):
	def __init__(self, url):
		self.url = url
		self.text = ''
		req = urllib2.Request(self.url)
		try:
			response = urllib2.urlopen(req)
			html = response.read()
			
			readable_article = Document(html).summary()
			readable_title = Document(html).title()
			soup = BeautifulSoup(readable_article,'lxml')
			self.text = soup.text
			
		except Exception as e:
			print "Article object creation problem"
	def get_text(self):
		return self.text
		
		
		
