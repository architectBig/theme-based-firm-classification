import csv
class WordCloudCSV(object):
	def __init__(self, csv):
		file_name = csv + '.csv'
		self.csv_file = open(file_name, 'a+')
	def append(self, word_cloud):
		writer = csv.writer(self.csv_file, quoting=csv.QUOTE_ALL)
		word_cloud_list_style = []
		word_cloud_list_style.append(word_cloud.firm_name)
		word_cloud_list_style.append(word_cloud.date)
		word_cloud_list_style.append(word_cloud.no_articles)
		for dic in word_cloud.frequency_distribution:
				word_cloud_list_style.append(dic[0])
				word_cloud_list_style.append(dic[1])
		writer.writerow(word_cloud_list_style)
		self.csv_file.close()
