from nltk.stem import WordNetLemmatizer
wordnet_lemmatizer = WordNetLemmatizer()
from nltk.stem.snowball import SnowballStemmer
print wordnet_lemmatizer.lemmatize('startups')

snowball_stemmer = SnowballStemmer("english")
print snowball_stemmer.stem('malwaretechnologies')
print snowball_stemmer.stem('malware')

import wordninja
import enchant

print wordninja.split('iit')
dictionary = enchant.Dict("en_US")
print dictionary.check("cyberattacks")
print dictionary.check("cyber")
print dictionary.check("attacks")
