class Theme(object):
	def __init__(self, file_name):
		self.theme_file = open(file_name, 'r')
	
	def load_theme_list(self):
		self.theme_list = self.theme_file.read().split('\n')
		if self.theme_list[len(self.theme_list)-1] == '':
			return self.theme_list[:-1]
		else:
			return self.theme_list
