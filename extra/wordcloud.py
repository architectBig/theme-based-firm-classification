from Tweet import Tweet
from nltk import word_tokenize, pos_tag
import enchant
from nltk.stem import WordNetLemmatizer as wnl
from nltk import FreqDist

class wordcloud (object):
	tweets = []
	def __init__(self, storageLocation, word_frequency, maximumTweets):
		self.storageLocation = storageLocation
		self.word_frequency = word_frequency
		self.maximumTweets = maximumTweets

	def tokenize(self, textList):
		#tokenize
		sentences = ''
		for text in textList:
			sentences = sentences + text
		words = word_tokenize(sentences)
		return words

	def pos_tagging(self, words):
		#adding parts of speech
		words_pp = pos_tag(words)
		return words_pp

	def discard_others(self, words_pp):
			new_words_pp = []
			dictionary = enchant.Dict('en_US')
			for word in words_pp:
				if word[1] == 'NN' or word[1] == 'NNP' or word[1] =='NNS' or word[1] =='JJ':
					if dictionary.check(word[0]) == True:
						new_words_pp.append(word)
			return new_words_pp

	def perform_lemmatization(self, words_without_pp):
		#lemmatize words
		wnlObj = wnl()
		lemmatize_word_list = []
		for word in words_without_pp:
			lword = wnlObj.lemmatize(word.lower())
			lemmatize_word_list.append(lword)
		return lemmatize_word_list
		
	def frequency_distribution (self, lemmatize_word_list):
		#frequency distribution
		frequency_of_words = FreqDist(lemmatize_word_list)
		word_with_frequency = frequency_of_words.most_common()
		return word_with_frequency
	def discard_lower_frequency_words(self, words_with_frequency):
		cloud_list_from_text = []
		for word in words_with_frequency:
			if word[1] >= self.word_frequency:
				cloud_list_from_text.append(word[0])
		return cloud_list_from_text
		
	def merge_tweet_text_cloud(self, searchQuery, hashtag_list, cloud_list_from_text):
		cloud_list = []
		hashtags = []
		for hashtag in hashtag_list:
			tags = hashtag.split()
			for tag in tags:
				tag = tag[1:]
				tag = tag.lower()
				if tag != searchQuery and tag != 'oldtweet' and tag != 'retweet':
					if tag not in hashtags:
						hashtags.append(tag)
		#lemmatize hashtags
		lemmatized_hashtags = self.perform_lemmatization(hashtags)
		for cloud_word in lemmatized_hashtags:
				cloud_list.append(cloud_word)
		for cloud_word in cloud_list_from_text:
			if cloud_word not in cloud_list:
				cloud_list.append(cloud_word)
		return cloud_list
	def discard_pp(self, NN_JJ_words):
		words_without_pp = []
		for word in NN_JJ_words:
			words_without_pp.append (word[0])
		return words_without_pp
	
	def getTweetsInfo(self):
		return wordcloud.tweets
	def getCloud(self, searchQuery, startDate, endDate):
		
		tweetObj = Tweet(startDate, endDate)
		wordcloud.tweets = tweetObj.getTweets(searchQuery, self.maximumTweets)
		words = self.tokenize(wordcloud.tweets[0])
		words_pp = self.pos_tagging(words)
		NN_JJ_words = self.discard_others(words_pp)
		words_without_pp = self.discard_pp(NN_JJ_words)
		lemmatize_word_list = self.perform_lemmatization(words_without_pp)
		words_with_frequency = self.frequency_distribution(lemmatize_word_list)
		cloud_list_from_text = self.discard_lower_frequency_words(words_with_frequency)
		cloud_list = []
		cloud_list.append(searchQuery)
		cloud_list.append(self.merge_tweet_text_cloud(searchQuery, wordcloud.tweets[1], cloud_list_from_text) )
		return cloud_list
