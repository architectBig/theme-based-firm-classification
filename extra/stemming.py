from nltk.stem.lancaster import LancasterStemmer as LS
from nltk import word_tokenize, pos_tag
from nltk import FreqDist
import got
import datetime as dt
import sys
lsObj = LS()


#gets date
today = dt.date.today().strftime("%Y-%m-%d")
# tweet collention
searchQuery = 'agingpeople'
tweetCriteria = got.manager.TweetCriteria().setQuerySearch(searchQuery).setSince("2013-01-01").setUntil(today).setMaxTweets(200)

#tokenize
words = word_tokenize("Societies have debts for the elderly. #Agingpeople #UNFPATurkey #UNFPA very much. Social policies for the elderly population must be developed continuously. #Agingpeople #UNFPATurkey #UNFPA. We mustn't forget our responsibilities to old people. #Agingpeople #UNFPATurkey #UNFPA")

#adding parts of speech
words_pp = pos_tag(words)

#add JJ and NN
new_words_pp = []
for word in words_pp:
	if word[1] == 'NN' or word[1] == 'NNP' or word[1] =='NNS' or word[1] =='JJ':
		new_words_pp.append(word)

#stemming
stem_word_list = []
for word in new_words_pp:
	stem = lsObj.stem(word[0])
	stem_word_list.append(stem)

#frequency distribution
frequency_of_words = FreqDist(stem_word_list)
word_and_frequency = frequency_of_words.most_common()
print word_and_frequency
