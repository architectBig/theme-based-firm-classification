from nltk.stem import WordNetLemmatizer as wnl
from nltk import word_tokenize, pos_tag
from nltk import FreqDist
import got
import datetime as dt
import sys
import enchant
def main():

	def isAscii(text):
		if isinstance(text, unicode):
			try:
				text.encode('ascii')
			except UnicodeEncodeError:
				return False
		else:
			try:
				text.decode('ascii')
			except UnicodeDecodeError:
				return False
		return True

	#object of wnl	
	wnlObj = wnl()
	#gets date
	today = dt.date.today().strftime("%Y-%m-%d")
	# tweet collention
	searchQuery = 'agingpeople'
	tweetCriteria = got.manager.TweetCriteria().setQuerySearch(searchQuery).setSince("2013-01-01").setUntil(today).setMaxTweets(200)
	tweets = got.manager.TweetManager.getTweets(tweetCriteria)

	sentences = ''
	for tweet in tweets:
		if isAscii(tweet.text) == True:
			sentences = sentences + tweet.text
	
	#tokenize
	words = word_tokenize(sentences)

	#adding parts of speech
	words_pp = pos_tag(words)

	#add JJ and NN
	new_words_pp = []
	dictionary = enchant.Dict('en_US')
	for word in words_pp:
		if word[1] == 'NN' or word[1] == 'NNP' or word[1] =='NNS' or word[1] =='JJ':
			if dictionary.check(word[0]) == True:
				new_words_pp.append(word)
	

	#lemmatize words
	lemmatize_list = []
	for word in new_words_pp:
		lword = wnlObj.lemmatize(word[0].lower())
		lemmatize_list.append(lword)

	#frequency distribution
	frequency_of_words = FreqDist(lemmatize_list)
	word_and_frequency = frequency_of_words.most_common()
	print word_and_frequency


if __name__ == '__main__':
	main()
