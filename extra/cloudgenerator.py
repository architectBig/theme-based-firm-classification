import got
import csv
import datetime as dt
import sys
from nltk import word_tokenize, pos_tag
import enchant
def main():
	
	def createThemeWord(key, value):
		themeword = []
		themeword.append (key)
		themeword.append(value)
		with open('themeword.csv', 'a') as out:
			writer = csv.writer(out, quoting=csv.QUOTE_ALL)
			writer.writerow(themeword)
	def addTags(hashtags):
		cheker = None
		hashtagList = hashtags.split()
		for hashtag in hashtagList:
			if searchQuery.lower() == hashtag.lower()[1:]:
				cheker = True
				break
		if cheker:
			hashtagList = hashtags.split()
		
			for hashtag in hashtagList:
				if hashtag.lower()[1:] not in globalTagList and hashtag.lower()[1:] != searchQuery and hashtag.lower()[1:] != 'oldpost' and hashtag.lower()[1:] != 'retweet':
					globalTagList.append(hashtag.lower()[1:])
				
	def isAscii(text):
		if isinstance(text, unicode):
			try:
				text.encode('ascii')
			except UnicodeEncodeError:
				return False
		else:
			try:
				text.decode('ascii')
			except UnicodeDecodeError:
				return False
		return True
	
	def getNouns(text):
		
		d = enchant.Dict('en_US')
		word_list = word_tokenize(text)
		word_list_partsOfSpeech = pos_tag(word_list)
	
		nounList = []
		adj = ''
		for word in word_list_partsOfSpeech:			
			if word[1] == "NN" or word[1] == "NNP" or word[1] == "NNS":		
				if d.check(word[0]) == True:			
					nounList.append(adj + ' ' + word[0])
					adj = ''
				else:
					print adj + ' ' + word[0]
					adj = ''
			elif word[1] == "JJ":
				adj = adj + ' ' + word[0]
			else:
				adj = ''
		return nounList
	def saveTweet(tweets, filename):
		
		with open(filename, 'w') as out:
			writer = csv.writer(out, quoting=csv.QUOTE_ALL)
			for tweet in tweets:
				if isAscii(tweet.text) == True:
					tweetList = []
					tweetList.append(tweet.text)
					tweetList.append(getNouns(tweet.text))
					tweetList.append(tweet.hashtags)
					addTags(tweet.hashtags)
					tweetList.append(tweet.date) 
					writer.writerow(tweetList)
		
	#Get date of today
	today = dt.date.today().strftime("%Y-%m-%d")
	globalTagList = []
	try:
		searchQuery = sys.argv[1][2:]
	
		try:
			searchQuery = searchQuery +' '+ sys.argv[2]
		except IndexError:
			searchQuery = searchQuery
			
		# Get tweets by query search
		print "Start mining ..."
		tweetCriteria = got.manager.TweetCriteria().setQuerySearch(searchQuery).setSince("2013-01-01").setUntil(today).setMaxTweets(200)
		tweets = got.manager.TweetManager.getTweets(tweetCriteria)
		saveTweet(tweets, searchQuery+".csv")
		createThemeWord(searchQuery, globalTagList)
		print "Result stored."
	except IndexError:
		print "Search parameter not found.\nTry python filename.py --searchquery"

if __name__ == '__main__':
	main()
	
