import csv
class CSV (object):
	def __init__ (self):
		x = 10
	def write(self, directory, filename, mode, content):
		with open(directory + filename, mode) as out:
			writer = csv.writer(out, quoting=csv.QUOTE_ALL)
			writer.writerow(content)
	def writeTweets(self, directory, filename, mode, tweetInfo):
		with open(directory + filename, mode) as out:
			writer = csv.writer(out, quoting=csv.QUOTE_ALL)
			
			tweetText = tweetInfo[0]
			tweetHash = tweetInfo[1]
			tweetDate = tweetInfo[2]
			length = len(tweetText)
			
			for i in range(0, length):
					tweetList = []
					tweetList.append(tweetText[i])
					tweetList.append(tweetHash[i])
					tweetList.append(tweetDate[i])
					writer.writerow(tweetList)
