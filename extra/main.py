from wordcloud import wordcloud
import datetime as dt
from writer import CSV
import sys
def main():
	try:
		length = len(sys.argv)
		length = length -1
		args = sys.argv[-length:]
		args[0] = args[0][2:]
		# Find today's date
		today = dt.date.today().strftime("%Y-%m-%d")
		csvObj = CSV()
		wordcloudObj = wordcloud("tweets/", 20, 700)
		print 'Generating Cloud...'
		for arg in args:
			searchQuery = arg

			# initial date, end date, maximum number of tweets
			cloud_list = wordcloudObj.getCloud(arg, "2010-01-01", today)
			
			csvObj.write('cloud/','themewordcloud.csv', 'a', cloud_list)
			tweet_List = wordcloudObj.getTweetsInfo()
			csvObj.writeTweets('tweets/',arg+".csv", 'w', tweet_List)
			print arg + " done"
		print 'Finished'
		
	except IndexError:
		print "Search parameter not found.\nTry python filename.py --searchquery"
if __name__ == '__main__':
	main()
