from themecloud.Tweet import Tweet
from themecloud.Themewordcloud import ThemewordCloud
from themecloud.ThemewordCloudCSV import ThemewordCloudCSV
from themecloud.Theme import Theme
from common.DATE import DATE

# create object of theme class
theme_obj = Theme('themecloud/themelist_1.txt')
# load theme list from theme class
theme_list = theme_obj.load_theme_list()

# create date obj
dateObj = DATE()

current = 1
print "Number of Theme: " + str(len(theme_list))
for theme in theme_list:
	# create theme word cloud obj
	themewordcloud_obj = ThemewordCloud(theme, dateObj.start_date, dateObj.end_date)
	# update the object with necessary information 
	themewordcloud_obj = themewordcloud_obj.generate_cloud_map()
	
	# convert the object into list
	thewordcloud_obj_list_style = []
	thewordcloud_obj_list_style.append(themewordcloud_obj.end_date)
	thewordcloud_obj_list_style.append(themewordcloud_obj.theme)
	thewordcloud_obj_list_style.append(themewordcloud_obj.no_tweets)
	for dic in themewordcloud_obj.cloud_map:
		if int(dic[1] > 9):
			thewordcloud_obj_list_style.append(dic[0])
			thewordcloud_obj_list_style.append(dic[1])

	# create CSV writer object
	themewordcloudcsv_obj = ThemewordCloudCSV('output_table/themeword/'+theme)
	# append list style of theme word cloud object into theme word cloud csv (csv writer)
	themewordcloudcsv_obj.append(thewordcloud_obj_list_style)
	
	print "	" + str(current) + " of " + str(len(theme_list)) + " completed " + "( " + theme+ " done.. )"
	current += 1


