from __future__ import division
from allocation.Comparision import SSSComparision
from operator import itemgetter
class ThemeFirmRelation(object):
	def __init__(self):
		self.theme_keywords = []
		self.firm_keywords = []
		self.relation_finder = SSSComparision()
	
	def get_relation(self, theme_keywords, firm_keywords):
		self.theme_keywords = theme_keywords
		self.firm_keywords = firm_keywords
		
		total_match = 0
		for theme_keyword in self.theme_keywords:
			similarity_score_list = []
			for firm_keyword in self.firm_keywords:
				current_score = self.relation_finder.compare(theme_keyword, firm_keyword)
				if current_score >=0 and current_score <= 1:
					similarity_score_list.append(((theme_keyword, firm_keyword),current_score))
		
			matched_list = [ tupple for tupple in sorted(similarity_score_list,key=itemgetter(1), reverse=True)[:1] if tupple[1] > 0]
	
			if len(matched_list) > 0:
				if matched_list[0][1] >= 0.2 and matched_list[0][1] < 0.25:
					total_match += 0.025
				elif matched_list[0][1] >= 0.25 and matched_list[0][1] < 0.30:
					total_match += 0.035
				elif matched_list[0][1] >= 0.3 and matched_list[0][1] < 0.35:
					total_match += 0.075
				elif matched_list[0][1] >= 0.35 and matched_list[0][1] < 0.4:
					total_match += 0.10
				elif matched_list[0][1] >= 0.4 and matched_list[0][1] < 0.45:
					total_match += 0.20
				elif matched_list[0][1] >= 0.45 and matched_list[0][1] < 0.5:
					total_match += 0.40
				elif matched_list[0][1] >= 0.5 and matched_list[0][1] < 0.55:
					total_match += 0.60
				elif matched_list[0][1] >= 0.55 and matched_list[0][1] < 0.6:
					total_match += 0.80
				elif matched_list[0][1] >= 0.60 and matched_list[0][1] < 0.7:
					total_match += 0.90
				elif matched_list[0][1] >= 0.7 and matched_list[0][1] < 0.8:
					total_match += 1.50	
				elif matched_list[0][1] >= 0.8:
					total_match += 2
				print '%s %s' % (matched_list[0][0], matched_list[0][1])

			
		return str((total_match/len(self.theme_keywords))*100)
		
		
		
