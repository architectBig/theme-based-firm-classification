import csv
class AllocationCloudCSV(object):
	def __init__(self, csv):
		file_name = csv
		self.csv_file = open(file_name, 'a+')
	def append(self, allocation_cloud):
		writer = csv.writer(self.csv_file, quoting=csv.QUOTE_ALL)
		writer.writerow(allocation_cloud)
		self.csv_file.close()
