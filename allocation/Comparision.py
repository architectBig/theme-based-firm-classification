import wordninja
import enchant
from requests import get
class SSSComparision(object):
	def __init__(self):
		self.dictionary = enchant.Dict("en_US")
		self.sss_url = "http://swoogle.umbc.edu/SimService/GetSimilarity"
		self.sss_type = 'relation'
		self.sss_corpus = 'webbase'
	def valid_eng_word(self,word):
		if self.dictionary.check(word) == True:
			return True
		else:
			return False
	def split_word(self, word):
		return " ".join(wordninja.split(word))
	
	def valid_words_after_split(self, word):
		word_list = wordninja.split(word)
		for word_1 in word_list:
			if len(word_1) == 1:
				return False
		return True
		
		
	def compare(self, s1, s2):
		if self.valid_words_after_split (s1) == True:
				s1 = self.split_word(s1)
		if self.valid_words_after_split (s2) == True:
				s2 = self.split_word(s2)
		try:
			response = get(self.sss_url, params={'operation':'api','phrase1':s1,'phrase2':s2,'type':self.sss_type,'corpus':self.sss_corpus})
			#print '%s, %s' % (s1,s2)
			return float(response.text.strip())
		except:
			print 'Error in getting similarity for %s: %s' % ((s1,s2), response)
			return 0.0
			
