class Wordcloud(object):
	def __init__(self):
		self.cloud_key = ''
		self.cloud_values = []
	
	def get_wordcloud(self, filename):
		with open(filename, "r") as f1:
			self.cloud_values = [word[1:len(word)-1] for word in f1.readlines()[-1].split(",")[3::2]]
		
		self.cloud_key = filename[filename.rfind('/'):filename.rfind('.')]
		
		return self.cloud_values
		
