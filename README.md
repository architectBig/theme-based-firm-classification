# README #


### What is this repository for? ###

* Classify firms on the basis of theme words.
* Firms are allocated to theme words like cyber security, robotics.
* For example, companies like IBM, Intel are allocated to theme cyber security by 70 percent, and 60 percent respectively

### Use guidelines ###

* Install python
* Clone this repository
* Use main_theme_cloud to construct theme word cloud for this current week
* Use main_firm_cloud with command line argument keyword to build firm word cloud for this current week
* Use main_allocation to bulid allocation cloud
* See output in the output_table directory

### Contributors? ###

* Marcel Mayer (IDP Supervisor)
