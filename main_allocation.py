from __future__ import division
from allocation.ThemeFirmRelation import ThemeFirmRelation
from allocation.Wordcloud import Wordcloud
from allocation.AllocationCloudCSV import AllocationCloudCSV
from common.DATE import DATE
from os import listdir
from os.path import isfile, join

date = DATE()
theme_files = [f for f in listdir('output_table/themecloud1/') if isfile(join('output_table/themecloud1/', f))]
firm_files = [f for f in listdir('output_table/firmcloud1/') if isfile(join('output_table/firmcloud1/', f))]
theme_firm_realtion = ThemeFirmRelation()
word_cloud = Wordcloud()


for theme_file in theme_files:
	allocation_csv = AllocationCloudCSV('output_table/allocationcloud/'+theme_file)
	allocation_cloud = []
	allocation_cloud.append(date.end_date)
	allocation_cloud.append(theme_file[:theme_file.rfind('.')])
	for firm_file in firm_files:
		allocation_cloud.append(firm_file[:firm_file.rfind('.')])
		allocation_cloud.append(theme_firm_realtion.get_relation(word_cloud.get_wordcloud('output_table/themecloud1/'+theme_file), word_cloud.get_wordcloud('output_table/firmcloud1/'+firm_file)))
		allocation_csv.append(allocation_cloud)
		#print theme_file[:theme_file.rfind('.')] + " " + firm_file[:firm_file.rfind('.')]
		#print theme_file[:theme_file.rfind('.')] + " " + firm_file[:firm_file.rfind('.')]+ theme_firm_realtion.get_relation(word_cloud.get_wordcloud('output_table/themecloud1/'+theme_file), word_cloud.get_wordcloud('output_table/firmcloud1/'+firm_file))




