from common.RSSFeed import RSSFeed
from common.Article import Article
from common.DATE import DATE
from common.TermDocumentMatrix import TermDocumentMatrix
from common.WordCloud import WordCloud
from common.WordCloudCSV import WordCloudCSV
import sys
try:
	frequecy_distribution_type = sys.argv[1]

	firm_list_file = open('common/firm_list/firm_list.txt', 'r')
	firm_list_file_content = firm_list_file.readlines()

	firm_list = [ line.strip().lower() for line in firm_list_file_content]

	date = DATE()

	count = 1
	for firm in firm_list:
	
		#ticker = firm.split('\t')[0].lower()
		#shortname = firm.split('\t')[1].lower()
		
		ticker = 'AAPL'.lower()
		shortname = 'Apple'.lower()
		
		print "Processing " + shortname + ' ( ' + str(count) + ' of ' + str(len(firm_list)) + ', type = '+ frequecy_distribution_type +' ) ' 
		rss_feed = RSSFeed('https://feeds.finance.yahoo.com/rss/2.0/headline?s=', ticker, date.end_date)
		article_text_list = []
	
		for article_links in rss_feed.get_articles():
			article = Article(article_links)
			article_text_list.append(article.get_text())

		tdm = TermDocumentMatrix(article_text_list)
		
		frequecy_distribution = []
		output_type = ''
		if frequecy_distribution_type == "co":
			frequecy_distribution = tdm.get_cooccured_firms_frequency_distribution(ticker, shortname)
			output_type = 'cooccurancecloud'
		elif frequecy_distribution_type == "keyword":
			frequecy_distribution = tdm.get_keywords_frequency_distribution()
			output_type = 'firmcloud'
		word_cloud = WordCloud(shortname, date.end_date, str(len(article_text_list)), frequecy_distribution)
		firm_cloud_csv = WordCloudCSV ('output_table/'+ output_type+'/' + shortname)
		firm_cloud_csv.append(word_cloud)
		count += 1
		break
except Exception as e:
	print e
